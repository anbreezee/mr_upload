package Applet.Threads;

public interface UrlTestThreadCallback {
    public void onError(String err);
    public void onGrantsSuccess(int maxUploadSize);
}
