package Applet.Threads;

import Applet.UI.Arrow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ArrowThread extends Thread {

    private final int MAX_ARROWS = 6;

    private ArrowThreadCallback callback;           // Callback
    private ArrayList<Arrow> arrows;
    private Arrow.Direction direction;
    private Timer timer;

    /**
     * @param callback ArrotThreadCallback
     */
    public ArrowThread(ArrowThreadCallback callback, Arrow.Direction direction) {
        super("ArrowThread");
        this.callback = callback;

        this.direction = direction;
        arrows = new ArrayList<Arrow>(MAX_ARROWS);
        for (int i = 0; i < MAX_ARROWS; i++) {
            arrows.add(generateArrow());
        }
    }

    /**
     * @return New random Arrow
     */
    private Arrow generateArrow() {
        Rectangle area;
        if (Math.random() < 0.5) {
            area = new Rectangle(0, 0, 150, 150);
        } else {
            area = new Rectangle(600, 0, 150, 150);
        }
        return new Arrow(area, direction);
    }

    /**
     * @param g Graphics context
     */
    public void paint(Graphics g) {
        for (int i = 0; i < arrows.size(); i++) {
            if (!arrows.get(i).update()) {
                arrows.set(i, generateArrow());
            }
        }

        int indexes[] = new int[MAX_ARROWS];
        for (int i = 0; i < MAX_ARROWS; i++) {
            indexes[i] = i;
        }
        for (int i = 0; i < MAX_ARROWS; i++) {
            for (int k = i; k < MAX_ARROWS; k++) {
                if (arrows.get(indexes[i]).getSize() > arrows.get(indexes[k]).getSize()) {
                    int t = indexes[i];
                    indexes[i] = indexes[k];
                    indexes[k] = t;
                }
            }
        }
        for (int i = 0; i < MAX_ARROWS; i++) {
            arrows.get(indexes[i]).draw(g);
        }
    }

    /**
     * Run function
     */
    @Override
    public void run() {
        ActionListener taskPerformer = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                callback.onArrowTick();
            }
        };
        timer = new Timer(30, taskPerformer);
        timer.start();
    }

    public void stopArrows() {
        timer.stop();
    }
}
