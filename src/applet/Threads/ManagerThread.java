package Applet.Threads;

import Applet.Context;
import Applet.Helpers.UiDialogHelper;
import Applet.Helpers.UrlHelper;
import Applet.Managers.HttpUploadManager;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import java.io.File;
import java.io.IOException;

public class ManagerThread extends Thread {

    private Context context;                        // Context
    private ManagerThreadCallback callback;         // Callback
    private JFileChooser fc = null;                 // File chooser
    public volatile boolean isPaused = false;       // Flag: paused
    public volatile boolean isCanceled = false;     // Flag: canceled
    public volatile boolean isForced = false;       // Flag: canceled

    /**
     * Constructor
     * @param context Applet context
     */
    public ManagerThread(Context context, ManagerThreadCallback callback) {
        super("ManagerThread");
        this.context = context;
        this.callback = callback;
        fc = new JFileChooser();
    }

    /**
     * Run function
     */
    @Override
    public void run() {
        String url;
        String ticket;
        String password = null;
        String srcFile;

        // We must test params
        url = context.getPolicy().getUrl();
        if (url.equals("")) {
            // UiDialogHelper.showErrorMessageBox(context, context.getPolicy().getI18nString("params_url_err"));
            reset();
            callback.onError(context.getPolicy().getI18nString("params_url_err"));
            return;
        }

        ticket = context.getPolicy().getTicket();
        if (ticket.equals("")) {
            // UiDialogHelper.showErrorMessageBox(context, context.getPolicy().getI18nString("params_ticket_err"));
            reset();
            callback.onError(context.getPolicy().getI18nString("params_ticket_err"));
            return;
        }

        // Construct upload url
        url = UrlHelper.constructUpUrl(context);

        // Firstly, user should select uploaded file
        try {
            srcFile = browseFile();
        } catch (IOException e) {
            // UiDialogHelper.showErrorMessageBox(context, e.getMessage());
            reset();
            callback.onError(e.getMessage());
            return;
        }
        if (null == srcFile || (new File(srcFile).isDirectory())) {
            UiDialogHelper.showWarningMessageBox(context,
                    context.getPolicy().getI18nString("dlg_file_err"),
                    context.getPolicy().getI18nString("dlg_file_title"));
            reset();
            return;
        }
        if (new File(srcFile).length() > context.getPolicy().getMaxUploadSize()) {
            UiDialogHelper.showWarningMessageBox(context,
                    String.format(
                            context.getPolicy().getI18nString("dlg_file_big"),
                            Encoder.utils.Format.readableFileSize(context.getPolicy().getMaxUploadSize())),
                    context.getPolicy().getI18nString("dlg_file_title"));
            reset();
            return;
        }

        // Secondly, user should enter the password
        do {
            password = promptPassword();
            if (password == null) {
                reset();
                return;
            }
            if (password.equals("")) {
                UiDialogHelper.showWarningMessageBox(context,
                        context.getPolicy().getI18nString("dlg_code_err"),
                        context.getPolicy().getI18nString("dlg_code_title"));
                password = null;
            }
        } while (null == password);

        // Store password for passlink generation
        context.password = password;

        // And then we are starting the upload
        try {
            String uploadStatus = uploadUrl(url, srcFile, password);

            JSONParser parser = new JSONParser();
            Object obj = parser.parse(uploadStatus);
            JSONObject json = (JSONObject) obj;

            String status = (String) json.get("status");
            if (!status.equals("ok")) {
                // UiDialogHelper.showErrorMessageBox(context, context.getPolicy().getI18nString("upload_failed"));
                reset();
                callback.onError(context.getPolicy().getI18nString("upload_failed"));
                return;
            }

            finished((String) json.get("url"));
        } catch (Exception e) {
            // UiDialogHelper.showErrorMessageBox(context,
            //         String.format("%s%n%s", context.getPolicy().getI18nString("upload_failed"), e.getMessage()));
            reset();
            callback.onError(e.getMessage());
        }
    }

    /**
     * Resets the upload
     */
    private void reset() {
        callback.onResetUpload();
    }

    /**
     * Called when upload is finished
     */
    private void finished(String url) {
        callback.onFinishUpload(url);
    }

    /**
     * Pauses the upload process
     */
    public void pauseUpload() {
        isPaused = true;
        isCanceled = false;
    }

    /**
     * Resumes the upload process
     */
    public void resumeUpload() {
        isPaused = false;
        isCanceled = false;
    }

    /**
     * Cancel the upload process
     */
    public void cancelUpload(boolean force) {
        isCanceled = true;
        isForced = force;
    }

    /**
     * Prompts password from user
     * @return Password or null, if dialog canceled
     */
    private String promptPassword() {
        JFrame frame = new JFrame("Password Dialog");
        final ImageIcon icon = new ImageIcon(getClass().getResource("/Images/MessageIcons/icon_password.png"));
        return (String) JOptionPane.showInputDialog(frame,
                context.getPolicy().getI18nString("dlg_code_mess"),
                context.getPolicy().getI18nString("dlg_code_title"),
                JOptionPane.WARNING_MESSAGE, icon, null, null);
    }

    /**
     * Starts "Browse for file" dialog
     * @return Path for selected directory or null, if dialog canceled
     */
    private String browseFile() throws IOException {
        fc.setDialogTitle(context.getPolicy().getI18nString("dlg_file_title"));
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setApproveButtonText("Select");
        int returnVal = fc.showOpenDialog(context.getApplet().getContentPane());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            return file.getCanonicalPath();
        }
        return null;
    }

    /**
     * Starts upload process
     * @param url Target url
     * @param srcFile Name of uploaded file
     * @param password Password for current download
     * @return Status of upload
     * @throws IOException
     */
    private String uploadUrl(String url, String srcFile, String password) throws IOException {
        HttpUploadManager connect = new HttpUploadManager(context, context, this, url, srcFile, password);
        return connect.run();
    }
}