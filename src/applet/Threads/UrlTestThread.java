package Applet.Threads;

import Applet.Context;
import Applet.Helpers.UiDialogHelper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class UrlTestThread extends Thread {

    private Context context;                    // Context
    private UrlTestThreadCallback callback;     // Callback
    private String url;                         // Test url

    /**
     * Constructor
     * @param context Applet context
     */
    public UrlTestThread(Context context, UrlTestThreadCallback callback, String url) {
        super("UrlTestThread");
        this.context = context;
        this.callback = callback;
        this.url = url;
    }

    /** Run function */
    @Override
    public void run() {
        try {
            String grants = testGrants();

            JSONParser parser = new JSONParser();
            Object obj = parser.parse(grants);
            JSONObject json = (JSONObject) obj;

            String status = (String) json.get("status");
            if (status.equals("ok")) {
                Long maxUploadSize = (Long) json.get("max_upload_size");
                callback.onGrantsSuccess(maxUploadSize.intValue());
            } else {
                callback.onNotGranted();
            }
        } catch (Exception e) {
            UiDialogHelper.showErrorMessageBox(context, e.getMessage());
        }
    }

    /**
     * @return Status
     * @throws java.io.IOException
     */
    public String testGrants() throws IOException {
        URLConnection connection = new URL(url + "?" + System.currentTimeMillis()).openConnection();
        connection.connect();
        InputStreamReader in = new InputStreamReader((InputStream) connection.getContent());
        BufferedReader buff = new BufferedReader(in);
        String line;
        String text = "";
        do {
            line = buff.readLine();
            if (line != null) {
                text += line + "\n";
            }
        } while (line != null);
        return text;
    }
}
