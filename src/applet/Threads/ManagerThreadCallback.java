package Applet.Threads;

public interface ManagerThreadCallback {
    public void onResetUpload();
    public void onFinishUpload(String url);
    public void onError(String err);
}
