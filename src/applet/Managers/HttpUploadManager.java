package Applet.Managers;

import Applet.Context;
import Applet.Helpers.UiDialogHelper;
import Applet.Threads.ManagerThread;
import Encoder.Encoder;
import Encoder.ProgressCallback;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.swing.JOptionPane;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public class HttpUploadManager {

    private Context context;                        // Context
    private ProgressCallback progressCallback;      // Progress callback
    private ManagerThread thread;                   // Thread
    private String urlTarget;                       // Target URL
    private String srcFile;                         // Source file
    private String password;                        // Password
    private HttpURLConnection connection  = null;   // Connection
    private FileInputStream reader = null;          // Input stream
    private OutputStream output;                    // Output stream
    private PrintWriter writer = null;              // Print writer
    private int szUpload;                           // Size of upload

    private String boundary = "MRUPLOAD" + Long.toHexString(System.currentTimeMillis());
    private String CRLF = "\r\n";
    private String charset = "UTF-8";

    /**
     * @param context Applet context
     * @param progressCallback Progress callback
     * @param url Target url
     * @param srcFile Temp filename
     * @param password Password
     */
    public HttpUploadManager(Context context, ProgressCallback progressCallback, ManagerThread thread,
                             String url, String srcFile, String password) {
        this.context = context;
        this.progressCallback = progressCallback;
        this.thread = thread;
        this.urlTarget = url;
        this.srcFile = srcFile;
        this.password = password;
    }

    /**
     * @return Status and target url
     * @throws IOException
     */
    public String run() throws IOException {
        open(urlTarget);
        szUpload = getUploadSize();
        openStreams(true, true);
        return upload(context.getPolicy().getTicket());
    }

    /**
     * @param url Target url
     * @throws IOException
     */
    public void open(String url) throws IOException {
        connection = (HttpURLConnection) new URL(url + "?" + System.currentTimeMillis()).openConnection();
        connection.setDoOutput(true);
        connection.setChunkedStreamingMode(32768);
        //connection.setRequestMethod("POST");
        connection.setRequestProperty("Connection", "keep-alive");
        // connection.setRequestProperty("Transfer-Encoding", "chunked");
        // connection.setRequestProperty("Content-Length", "706");
        // connection.setFixedLengthStreamingMode(706);
        connection.setRequestProperty("Cache-Control", "max-age=0");
        connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
    }

    public void disconnect() {
        if (!this.isConnected()) return;
        connection.disconnect();
        connection = null;
    }

    /**
     * @return Upload size
     * @throws IOException
     */
    public int getUploadSize() throws IOException {
        File file = new File(srcFile);
        if (!file.exists() || file.isDirectory()) {
            throw new IOException();
        }
        return (int) file.length();
    }

    /**
     * @throws IOException
     */
    public void openStreams(Boolean out, Boolean in) throws IOException {
        if (out && writer == null) {
            try {
                //output = new FileOutputStream("c:\\test\\test.jav");
                output = connection.getOutputStream();
                writer = new PrintWriter(new OutputStreamWriter(output, charset), true);
            } catch(IOException e) {
                URLConnection httpConnection = connection;
                int code = ((HttpURLConnection) httpConnection).getResponseCode();
                throw new IOException("Error " + String.valueOf(code));
            }
        }
        if (in && reader == null) {
            reader = new FileInputStream(srcFile);
        }
    }

    /**
     * @return Connection status
     */
    public boolean isConnected() {
        return (null != connection);
    }

    /**
     * @return Response string
     * @throws IOException
     */
    public String upload(String ticket) throws IOException {
        try {
            String filename = new File(srcFile).getName();

            writer.append(item("ticket", ticket));

            writer.append(header()).flush();
            Encoder.initialize(reader, output, szUpload, password, filename, "", progressCallback);
            Encoder.updateHeader();
            while (0 < Encoder.updateData()) {
                while (waiting()) {}
            }
            Encoder.updateHMAC();
            writer.append(footer()).flush();

            int responseCode = connection.getResponseCode();
            if (responseCode >= 400) {
                InputStreamReader in = new InputStreamReader(connection.getErrorStream());
                BufferedReader buff = new BufferedReader(in);
                String line;
                String text = "";
                do {
                    line = buff.readLine();
                    if (line != null) { text += line + "\n"; }
                } while (line != null);

                connection.disconnect();

                JSONParser parser = new JSONParser();
                try {
                    Object obj = parser.parse(text);
                    JSONObject jsonObject  = (JSONObject) obj;
                    String err = (String) jsonObject.get("text");
                    throw new IOException(err);
                } catch (ParseException e) {
                    throw new IOException("Error");
                }
            }

            // Get the input stream for the HTML portion
            String responseString = "";
            InputStream response = connection.getInputStream();
            Scanner in = new Scanner (response);

            while (in.hasNextLine()) {
                responseString += in.nextLine();
            }

            Encoder.close();
            disconnect();

            return responseString;
        } catch (Exception e) {
            Encoder.close();
            disconnect();
            throw new IOException(e.getMessage());
        }
    }

    /**
     * @return <...>
     * @throws InterruptedException
     * @throws IOException
     */
    private boolean waiting() throws InterruptedException, IOException {

        // While upload paused, wait...
        while (thread.isPaused && !thread.isCanceled) {
            Thread.sleep(100);
            Thread.yield();
        }

        // If upload canceled, then exit
        if (thread.isCanceled) {
            thread.isCanceled = false;
            if (thread.isForced) {
                Encoder.close();
                throw new IOException(context.getPolicy().getI18nString("upload_int"));
            } else {
                int response = UiDialogHelper.showConfirmMessageBox(context,
                        context.getPolicy().getI18nString("dlg_cancel_mess"),
                        context.getPolicy().getI18nString("dlg_cancel_title"),
                        JOptionPane.YES_NO_OPTION);
                if (JOptionPane.YES_OPTION == response) {
                    throw new IOException(context.getPolicy().getI18nString("upload_int"));
                } else {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param data <...>
     * @return <...>
     * @throws IOException
     */
    /*
    public String upload(String data) throws IOException {
        try {
            writer.append(header()).flush();
            writer.append(data);
            writer.append(footer()).flush();

            // Get the input stream for the HTML portion
            String responseString = "";
            InputStream response = connection.getInputStream();
            Scanner in = new Scanner (response);

            while (in.hasNextLine()) {
                responseString += in.nextLine();
            }

            disconnect();

            return responseString;
        } catch (Exception e) {
            disconnect();
            throw new IOException(e.getMessage());
        }
    }              */

    /**
     * @return header
     */
    private String header() {
        return "--" + boundary + CRLF
                + "Content-Disposition: form-data; name=\"file\"; filename=\"upload\"" + CRLF
                + "Content-Type: application/octet-stream" + CRLF
                + CRLF;
    }

    /**
     * @return footer
     */
    private String footer() {
        return CRLF + "--" + boundary + "--" + CRLF;
    }

    /**
     * @param name <...>
     * @param value <...>
     * @return <...>
     */
    private String item(String name, String value) {
        return "--" + boundary + CRLF
                + "Content-Disposition: form-data; name=\"" + name + "\"" + CRLF
                + CRLF
                + value + CRLF;
    }
}