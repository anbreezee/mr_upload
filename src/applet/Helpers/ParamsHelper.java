package Applet.Helpers;

import Applet.Context;

public class ParamsHelper {

    /**
     * Returns parameter value as string
     * @param key Name of parameter
     * @param def Default string value
     * @return Parameter value as string
     */
    public static String getParameter(Context context, String key, String def) {
        return (context.getApplet().getParameter(key) != null
                ? context.getApplet().getParameter(key) : def);
    }

    /**
     * Returns parameter value as integer
     * @param key Name of parameter
     * @param def Default integer value
     * @return Parameter value as integer
     */
    public static int getParameter(Context context, String key, int def) {
        String paramDef = Integer.toString(def);
        String paramStr = context.getApplet().getParameter(key) != null
                ? context.getApplet().getParameter(key) : paramDef;
        return parseInt(paramStr, def);
    }

    /**
     * Returns parameter value as float
     * @param key Name of parameter
     * @param def Default float value
     * @return Parameter value as float
     */
    public static float getParameter(Context context, String key, float def) {
        String paramDef = Float.toString(def);
        String paramStr = context.getApplet().getParameter(key) != null
                ? context.getApplet().getParameter(key) : paramDef;
        return parseFloat(paramStr, def);
    }

    /**
     * Returns parameter value as long
     * @param key Name of parameter
     * @param def Default long value
     * @return Parameter value as long
     */
    public static long getParameter(Context context, String key, long def) {
        String paramDef = Long.toString(def);
        String paramStr = context.getApplet().getParameter(key) != null
                ? context.getApplet().getParameter(key) : paramDef;
        return parseLong(paramStr, def);
    }

    /**
     * Returns parameter value as boolean
     * @param key Name of parameter
     * @param def Default boolean value
     * @return Parameter value as boolean
     */
    public static boolean getParameter(Context context, String key, boolean def) {
        String paramDef = (def ? "true" : "false");
        String paramStr = context.getApplet().getParameter(key) != null
                ? context.getApplet().getParameter(key) : paramDef;
        return parseBoolean(paramStr, def);
    }

    /**
     * Parses string to integer
     * @param value String value
     * @param def Default integer value
     * @return Result integer value
     */
    public static int parseInt(String value, int def) {
        int ret;
        try {
            ret = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            ret = def;
        }
        return ret;
    }

    /**
     * Parses string to float
     * @param value String value
     * @param def Default float value
     * @return Result float value
     */
    public static float parseFloat(String value, float def) {
        float ret;
        try {
            ret = Float.parseFloat(value);
        } catch (NumberFormatException e) {
            ret = def;
        }
        return ret;
    }

    /**
     * Parses string to long
     * @param value String value
     * @param def Default long value
     * @return Result long value
     */
    public static long parseLong(String value, long def) {
        long ret;
        try {
            ret = Long.parseLong(value);
        } catch (NumberFormatException e) {
            ret = def;
        }
        return ret;
    }

    /**
     * Parses string to boolean
     * @param value String value
     * @param def Default boolean value
     * @return Result boolean value
     */
    public static boolean parseBoolean(String value, boolean def) {
        return value.toUpperCase().equals("TRUE") || !value.toUpperCase().equals("FALSE") && def;
    }
}