package Applet.Helpers;

import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
import com.sun.org.apache.xml.internal.security.utils.Base64;

public class UrlHelper {

    /**
     * Decodes pseudo-BASE64
     * @param data pseudo-BASE64
     * @return Source string
     */
    public static String decodeUrlSafe(String data) throws Exception {
        String result;
        try {
            result = new String(Base64.decode(data.replace('-', '+').replace('_', '/').replace(",", "=")));
        } catch (Base64DecodingException e) {
            throw new Exception("Error during BASE64 decoding.");
        }
        return result;
    }

    /**
     * Encodes to pseudo-BASE64
     * @param data String
     * @return pseudo-BASE64
     */
    public static String encodeUrlSafe(byte[] data) {
        return Base64.encode(data).replace('+', '-').replace('/', '_').replaceAll("=", ",");
    }

    /**
     * Returns URL part from PassLink
     * @param url PassLink url
     * @return URL address
     * @throws Exception
     */
    public static String getUrlFromPassLink(String url) throws Exception {
        String[] parts = url.split("[:]");
        if (parts.length < 3) throw new Exception("Invalid URL");
        return parts[0] + ":" + parts[1] + ":" + parts[2]; // http://...:443/dl/...
    }

    /**
     * Returns PASS part from PassLink
     * @param url PassLink url
     * @return PASS
     * @throws Exception
     */
    public static String getPassFromPassLink(String url) throws Exception {
        String[] parts = url.split("[:]");
        if (parts.length < 4) throw new Exception("Invalid URL");
        return decodeUrlSafe(parts[3]);
    }

    /**
     * Returns true, if URL is a PassLink
     * @param url URL string
     * @return result
     */
    public static boolean isUrlPassLink(String url) {
        String[] parts = url.split("[:]");
        return (parts.length > 3);
    }
}
