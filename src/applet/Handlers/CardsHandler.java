package Applet.Handlers;

import Applet.Context;
import Applet.UI.*;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.CardLayout;

public class CardsHandler {

    private Context context;                    // Context

    private JPanel cards;                       // Cards
    private PanelWait panelWait;                // Panel: Wait
    private PanelNotGranted panelNotGranted;    // Panel: Not Granted
    private PanelStart panelStart;              // Panel: Start
    private PanelUpload panelUpload;            // Panel: Upload
    private PanelFinish panelFinish;            // Panel: Finish
    private PanelTryAgain panelTryAgain;    // Panel: Try Again

    /**
     * Constructor
     * @param context Context
     */
    public CardsHandler(Context context) {
        this.context = context;
    }

    /**
     * Constructs user interface
     */
    public void constructUI() {
        // Cards
        cards = new JPanel(new CardLayout());
        cards.setBackground(Color.white);

        // Card: Wait
        panelWait = new PanelWait(context);
        cards.add(panelWait, "panelWait");

        // Card: Not granted
        panelNotGranted = new PanelNotGranted(context);
        cards.add(panelNotGranted, "panelNotGranted");

        // Card: Start
        panelStart = new PanelStart(context, context);
        cards.add(panelStart, "panelStart");

        // Card: Upload
        panelUpload = new PanelUpload(context, context);
        cards.add(panelUpload, "panelUpload");

        // Card: Finish
        panelFinish = new PanelFinish(context);
        cards.add(panelFinish, "panelFinish");

        // Card: Try Again
        panelTryAgain = new PanelTryAgain(context, context);
        cards.add(panelTryAgain, "panelTryAgain");

        context.getApplet().getContentPane().add(cards);
    }

    /**
     * Shows specified card
     * @param name Panel name
     */
    public void showCard(String name) {
        CardLayout cl = (CardLayout) cards.getLayout();
        if (name.equals("wait"))            { cl.show(cards, "panelWait");      }
        else if (name.equals("notgranted")) { cl.show(cards, "panelNotGranted");}
        else if (name.equals("start"))      { cl.show(cards, "panelStart");     }
        else if (name.equals("upload"))     { cl.show(cards, "panelUpload");    }
        else if (name.equals("finish"))     { cl.show(cards, "panelFinish");    }
        else if (name.equals("tryagain"))   { cl.show(cards, "panelTryAgain");  }
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // UPLOAD PANEL
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Resets upload panel
     */
    public void uploadPanelReset() {
        panelUpload.reset();
    }

    /**
     * Updates upload progress
     * @param value <...>
     * @param description <...>
     */
    public void uploadPanelUpdateProgress(int value, String description) {
        panelUpload.updateProgress(value, description);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // FINISH PANEL
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * finishPanelInitialize()
     * Initializes finish panel
     * @param url URL
     */
    public void finishPanelInitialize(String url, String message) {
        panelFinish.initialize(url, message);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Try Again panel
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Initializes try again panel
     * @param error Error string
     */
    public void tryAgainPanelInitialize(String error) {
        panelTryAgain.initialize(error);
    }
}