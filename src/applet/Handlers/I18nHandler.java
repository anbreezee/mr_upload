package Applet.Handlers;

import java.util.Locale;
import java.util.ResourceBundle;

public class I18nHandler {

    private ResourceBundle messages;    // Resource bundle

    /**
     * Constructor
     */
    public I18nHandler() {
        Locale currentLocale = new Locale("en", "US");
        messages = ResourceBundle.getBundle("lang", currentLocale);
    }

    /**
     * @param key Key
     * @return String by key
     */
    public String getString(String key) {
        return messages.getString(key);
    }
}