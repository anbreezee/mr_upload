package Applet;

import Applet.Handlers.I18nHandler;
import Applet.Helpers.ParamsHelper;

public class Policy {

    private Context context;    // Context
    private I18nHandler i18n;   // Internationalization handler
    private String url;         // URL
    private String ticket;      // Ticket
    private int maxUploadSize;  // Max upload size

    private static final String PARAM_URL = "url";
    private static final String PARAM_URL_DEFAULT = "";
    ///private static final String PARAM_URL_DEFAULT = "http://31.204.128.90:8081";

    private static final String PARAM_TICKET = "ticket";
    private static final String PARAM_TICKET_DEFAULT = "";
    // private static final String PARAM_TOKEN_DEFAULT = "f1f2a3aed15dd71a28fc8ae13e7384741a312b5c7da570731a6b647bd9a00188";

    /**
     * Constructor
     * @param context Context
     */
    public Policy(Context context) {
        this.context = context;
        i18n = new I18nHandler();
        parseParams();
    }

    /**
     * Parses applet parameters
     */
    private void parseParams() {
        url = ParamsHelper.getParameter(context, PARAM_URL, PARAM_URL_DEFAULT);
        ticket = ParamsHelper.getParameter(context, PARAM_TICKET, PARAM_TICKET_DEFAULT);
    }

    /**
     * Returns localized string
     * @param key String key
     * @return Localized string
     */
    public String getI18nString(String key) {
        return i18n.getString(key);
    }

    /**
     * @return Url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url URL
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return Ticket
     */
    public String getTicket() {
        return ticket;
    }

    /**
     * @return Max Upload Size
     */
    public int getMaxUploadSize() {
        return maxUploadSize;
    }

    /**
     * @param maxUploadSize Max Upload Size
     */
    public void setMaxUploadSize(int maxUploadSize) {
        this.maxUploadSize = maxUploadSize;
    }
}