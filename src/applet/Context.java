package Applet;

import Applet.Handlers.CardsHandler;
import Applet.Helpers.UiDialogHelper;
import Applet.Helpers.UrlHelper;
import Applet.Threads.*;
import Applet.UI.Arrow;
import Applet.UI.PanelStartCallback;
import Applet.UI.PanelTryAgainCallback;
import Applet.UI.PanelUploadCallback;
import Encoder.ProgressCallback;
import netscape.javascript.JSObject;

import javax.swing.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;
import java.io.UnsupportedEncodingException;

public class Context implements UrlTestThreadCallback, ManagerThreadCallback, ArrowThreadCallback,
        ClipboardOwner, ProgressCallback, PanelStartCallback, PanelUploadCallback, PanelTryAgainCallback {

    private Applet applet;                 // Applet
    private Policy policy;                  // Policy
    private CardsHandler cardsHandler;      // Cards handler
    private ManagerThread uploadThread;     // Upload Manager thread
    private ArrowThread arrowThread;        // Arrows!

    public String password;

    /**
     * Constructor
     * @param applet Applet
     */
    public Context(Applet applet) {
        this.applet = applet;
        policy = new Policy(this);
        cardsHandler = new CardsHandler(this);
    }

    /**
     * @return Applet
     */
    public Applet getApplet() {
        return applet;
    }

    /**
     * return Policy
     */
    public Policy getPolicy() {
        return policy;
    }

    /**
     * Constructs user interface
     */
    public void constructUI() {
        cardsHandler.constructUI();
    }

    /**
     * Starts logic
     */
    public void start() {
        arrowThread = new ArrowThread(this, Arrow.Direction.UP);
        arrowThread.start();
        _wait();
        testGrants();
    }

    /**
     * Tests grants
     */
    public void testGrants() {
        String url = policy.getUrl();
        if (url.equals("")) {
            UiDialogHelper.showErrorMessageBox(this, policy.getI18nString("params_url_err"));
            stopArrows();
            _notgranted();
            return;
        }
        (new UrlTestThread(this, this)).start();
    }

    /**
     * Updates upload progress
     * @param value Progress value
     * @param description Progress description
     */
    public void updateProgress(final int value, final String description) {
        // Return, if paused...
        if (null != uploadThread && uploadThread.isPaused) { return; }

        // Update progress
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                cardsHandler.uploadPanelUpdateProgress(value, description);
            }
        });
    }

    /**
     * @param url <...>
     * @return <...>
     * @throws UnsupportedEncodingException
     */
    public String getPassLink(String url) throws UnsupportedEncodingException {
        return url + ":" + UrlHelper.encodeUrlSafe(password.getBytes("UTF-8"));
    }

    public void stopArrows() {
        arrowThread.stopArrows();
    }

    public void cancelUpload() {
        uploadThread.cancelUpload(true);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // CALLBACKS
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onError(String err) {
        stopArrows();
        if (err.equals("No ticket")) {
            err = "There is no ticket for upload now. Please try again.";
        }
        _error(err);
    }

    /**
     * @param maxUploadSize Max upload size
     */
    @Override
    public void onGrantsSuccess(int maxUploadSize) {
        policy.setMaxUploadSize(maxUploadSize);
        _start();
    }

    @Override
    public void onResetUpload() {
        _start();
    }

    /**
     * @param url Target url
     */
    @Override
    public void onFinishUpload(String url) {
        _finish(url);
    }

    /**
     * Starts the upload process
     */
    @Override
    public void onStartUploadClicked() {
        stopArrows();
        _upload();
        uploadThread = new ManagerThread(this, this);
        uploadThread.start();
    }

    /**
     * Pauses the upload process
     */
    @Override
    public void onPauseUploadClicked() {
        uploadThread.pauseUpload();
    }

    /**
     * Resumes the upload process
     */
    @Override
    public void onResumeUploadClicked() {
        uploadThread.resumeUpload();
    }

    /**
     * Cancel the upload process
     */
    @Override
    public void onCancelUploadClicked() {
        uploadThread.cancelUpload(false);
    }

    @Override
    public void onArrowTick() {
        this.getApplet().repaintArrows(this.arrowThread);
    }

    @Override
    public void onTryAgainClicked() {
        // Reload page
        try {
            JSObject win = JSObject.getWindow(applet);
            win.call("reloadApplet", null);
        } catch (Exception ex) {
            // OOPS!
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Clipboard
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param clipboard <...>
     * @param contents <...>
     */
    @Override
    public void lostOwnership(Clipboard clipboard, Transferable contents) {}


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // GRAPH NODES
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Shows error panel
     * @param err Error string
     */
    public void _error(String err) {
        cardsHandler.tryAgainPanelInitialize(err);
        cardsHandler.showCard("tryagain");
    }

    /**
     * Shows not granted panel
     */
    public void _notgranted() {
        cardsHandler.showCard("notgranted");
    }

    /**
     * Shows start upload panel
     */
    public void _start() {
        cardsHandler.showCard("start");
    }

    /**
     * @param url <...>
     * Show finish upload panel
     */
    public void _finish(String url) {
        String pass_link_url = "";
        try {
            pass_link_url = getPassLink(url);
        } catch (UnsupportedEncodingException e) {
            UiDialogHelper.showErrorMessageBox(this, e.getMessage());
        }
        cardsHandler.finishPanelInitialize(url,
                String.format(policy.getI18nString("upload_success"), url, url, pass_link_url, pass_link_url));
        cardsHandler.showCard("finish");
    }

    /**
     * Show upload progress panel
     */
    public void _upload() {
        cardsHandler.uploadPanelReset();
        cardsHandler.showCard("upload");
        cardsHandler.uploadPanelUpdateProgress(0, "Initializing...");
    }

    /**
     * Show wait panel
     */
    public void _wait() {
        cardsHandler.showCard("wait");
    }
}
