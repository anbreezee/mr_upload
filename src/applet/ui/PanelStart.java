package Applet.UI;

import Applet.Context;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.Box;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PanelStart extends JPanel implements ActionListener {

    private Context context;                // Context
    private PanelStartCallback callback;    // Callback
    private JButton btnUpload;              // UI: Upload button

    /**
     * Constructor
     * @param context <...>
     * @param callback <...>
     */
    public PanelStart(Context context, PanelStartCallback callback) {
        this.context = context;
        this.callback = callback;

        this.setBackground(Color.white);
        this.setLayout(new BorderLayout());

        final ImageIcon icon = new ImageIcon(getClass().getResource("/Images/Buttons/btn_upload.png"));

        btnUpload = new JButton(icon);
        btnUpload.setBorderPainted(false);
        btnUpload.setContentAreaFilled(false);
        btnUpload.setFocusPainted(false);
        btnUpload.addActionListener(this);
        btnUpload.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

        Box bv = Box.createVerticalBox();
        Box bh = Box.createHorizontalBox();
        bh.add(Box.createGlue());
        bh.add(btnUpload);
        bh.add(Box.createGlue());
        bv.add(bh);
        this.add(bv, BorderLayout.CENTER);
    }

    /**
     * @param e <...>
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnUpload) {
            callback.onStartUploadClicked();
        }
    }
}