package Applet.UI;

public interface PanelUploadCallback {
    public void onPauseUploadClicked();
    public void onResumeUploadClicked();
    public void onCancelUploadClicked();
}
