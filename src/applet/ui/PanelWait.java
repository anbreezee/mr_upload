package Applet.UI;

import Applet.Context;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.Box;
import java.awt.Color;
import java.awt.BorderLayout;

public class PanelWait extends JPanel {

    private Context context;    // Context
    private JLabel lblWait;     // UI: Label wait

    /**
     * Constructor
     * @param context Context
     */
    public PanelWait(Context context) {
        this.context = context;

        this.setBackground(Color.white);
        this.setLayout(new BorderLayout());

        final ImageIcon icon = new ImageIcon(getClass().getResource("/Images/PreLoaders/standard.gif"));

        lblWait = new JLabel();
        lblWait.setText(context.getPolicy().getI18nString("please_wait"));
        lblWait.setIcon(icon);
        lblWait.setHorizontalAlignment(JLabel.CENTER);

        Box bv = Box.createVerticalBox();
        Box bh = Box.createHorizontalBox();
        bh.add(Box.createGlue());
        bh.add(lblWait);
        bh.add(Box.createGlue());
        bv.add(bh, BorderLayout.CENTER);
        this.add(bv, BorderLayout.CENTER);
    }
}
