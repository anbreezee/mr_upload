package Applet.UI;

import Applet.Context;
import Applet.Helpers.UiDialogHelper;

import javax.swing.JPanel;
import javax.swing.JEditorPane;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JTextPane;
import javax.swing.Box;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.UnsupportedEncodingException;

public class PanelFinish extends JPanel implements ActionListener {

    private Context context;                // Context
    private JEditorPane edComplete;         // UI: Complete message
    private JButton btnClipboard;           // UI: Copy Link to clipboard button
    private JButton btnPassLinkClipboard;   // UI: Copy PassLink to clipboard button
    private String url;                     // URL

    /**
     * Constructor
     * @param context Context
     */
    public PanelFinish(Context context) {
        this.context = context;

        this.setBackground(Color.white);
        this.setLayout(new BorderLayout());

        JTextPane f = new JTextPane();
        f.setContentType("text/html");
        f.setText("<html>Hello World</html>");
        f.setEditable(false);
        f.setBackground(null);
        f.setBorder(null);

        edComplete = new JEditorPane();
        edComplete.setEditable(false);
        edComplete.setContentType("text/html");

        final ImageIcon iconClipboard = new ImageIcon(
                getClass().getResource("/Images/Buttons/btn_clipboard.png"));
        btnClipboard = new JButton(iconClipboard);
        btnClipboard.setBorderPainted(false);
        btnClipboard.setContentAreaFilled(false);
        btnClipboard.setFocusPainted(false);
        btnClipboard.addActionListener(this);
        btnClipboard.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

        final ImageIcon iconPassLinkClipboard = new ImageIcon(
                getClass().getResource("/Images/Buttons/btn_passlink_clipboard.png"));
        btnPassLinkClipboard = new JButton(iconPassLinkClipboard);
        btnPassLinkClipboard.setBorderPainted(false);
        btnPassLinkClipboard.setContentAreaFilled(false);
        btnPassLinkClipboard.setFocusPainted(false);
        btnPassLinkClipboard.addActionListener(this);
        btnPassLinkClipboard.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

        Box bv = Box.createVerticalBox();
        Box bh = Box.createHorizontalBox();
        Box bv_ = Box.createVerticalBox();
        bv_.add(btnClipboard);
        bv_.add(btnPassLinkClipboard);
        bh.add(Box.createGlue());
        bh.add(edComplete);
        bh.add(bv_);
        bh.add(Box.createGlue());
        bv.add(bh, BorderLayout.CENTER);
        this.add(bv, BorderLayout.CENTER);
    }

    /**
     * Initialization function
     * @param url URL
     */
    public void initialize(String url, String message) {
        edComplete.setText(message);
        this.url = url;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == btnClipboard  ) {
            setClipboardContents(url);
            UiDialogHelper.showSuccessMessageBox(context,
                    context.getPolicy().getI18nString("link_copied_to_clipboard"));
        } else if (ae.getSource() == btnPassLinkClipboard) {
            try {
                setClipboardContents(context.getPassLink(url));
                UiDialogHelper.showSuccessMessageBox(context,
                        context.getPolicy().getI18nString("passlink_copied_to_clipboard"));
            } catch (UnsupportedEncodingException e) {
                UiDialogHelper.showErrorMessageBox(context, e.getMessage());
            }
        }
    }

    /**
     * Place a String on the clipboard, and make this class the
     * owner of the Clipboard's contents.
     */
    public void setClipboardContents(String aString){
        StringSelection stringSelection = new StringSelection(aString);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, context);
    }
}