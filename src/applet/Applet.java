package Applet;

import Applet.Threads.ArrowThread;
import netscape.javascript.JSObject;

import javax.swing.JApplet;
import javax.swing.UIManager;
import java.awt.*;

public class Applet extends JApplet {

    Context context;
    Image offScreenBuffer = null;

    /**
     * Called each time the applet is shown on the web page
     */
    @Override
    public void init() {

        // Set Nimbus theme here...
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception ignored) {}

        // and construct user interface
        this.context = new Context(this);
        context.constructUI();
        context.start();

        // Hide applet loading flash
        try {
            JSObject win = JSObject.getWindow(Applet.this);
            win.call("showApplet", null);
        } catch (Exception ignored) {}
    }

    /**
     * Called each time the applet is shown on the web page
     */
    @Override
    public void start() {}

    /**
     * @see java.applet.Applet#stop()
     */
    @Override
    public void stop() {
        this.context.stopArrows();
    }

    /**
     * @see java.applet.Applet#destroy()
     */
    @Override
    public void destroy() {
        context.cancelUpload();
    }

    /**
     * @see java.applet.Applet#getParameterInfo()
     */
    public String[][] getParameterInfo() {
        return null;
    }

    /**
     * @see java.applet.Applet#getAppletInfo()
     */
    public String getAppletInfo() {
        return "Upload Applet";
    }

    /**
     * @param arrowThread Arrows thread
     */
    public void repaintArrows(ArrowThread arrowThread) {
        if (offScreenBuffer == null ||
                (!(offScreenBuffer.getWidth(this) == getSize().width
                        && offScreenBuffer.getHeight(this) == getSize().height)))
        {
            offScreenBuffer = this.createImage(getSize().width, getSize().height);
        }

        Graphics gr = offScreenBuffer.getGraphics();

        // blank the canvas
        gr.setColor(Color.WHITE);
        gr.fillRect(0,0,offScreenBuffer.getWidth(this),offScreenBuffer.getHeight(this));

        // Draw arrows
        this.paint(gr);
        arrowThread.paint(gr);

        // Swtich screen buffer
        this.getGraphics().drawImage(offScreenBuffer, 0, 0, this);
    }
}