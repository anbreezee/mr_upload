package Encoder;

import Encoder.crypt.AES;
import Encoder.crypt.HMAC;
import Encoder.io.Reader;
import Encoder.io.Writer;
import Encoder.utils.Format;

import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

import static Encoder.utils.Format.readableFileSize;

public class Decoder {

    private static String mVersion;
    private static String mPassword;
    private static String mFileName;
    private static String mTitle;
    private static long mFileSize;
    private static long mDataSize;
    private static long mProcessedSize;
    private static byte[] mSaltAES;

    private static ProgressCallback callback;

    /**
     * Initialization
     * @param in input stream
     * @param out output stream
     * @param fileSize total bytes in input stream
     * @param password password
     * @throws InvalidKeyException
     * @throws InvalidKeySpecException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    public static void initialize(InputStream in, OutputStream out, long fileSize, String password,
                                  ProgressCallback callback)
    throws InvalidKeyException, InvalidKeySpecException, InvalidAlgorithmParameterException,
           NoSuchAlgorithmException, NoSuchPaddingException {
        mFileSize = fileSize;
        mPassword = password;
        Decoder.callback = callback;

        callback.updateProgress(0, "Initializing...");
        Reader.init(in);
        Writer.init(out);
        HMAC.init(password);
        AES.init(AES.ENCODE, password);
        callback.updateProgress(1, "Starting... Please wait");
    }

    /**
     * Reads header and initializes AES & HMAC
     * @throws IOException
     * @throws InvalidKeyException
     * @throws InvalidKeySpecException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     */
    public static void updateHeader()
    throws IOException, InvalidKeyException, InvalidAlgorithmParameterException, InvalidKeySpecException,
           NoSuchPaddingException, NoSuchAlgorithmException {

        int readBytes;
        int osVersion = 0;
        int szVersion = Config.VERSION.length();
        int osIV = osVersion + szVersion;
        int szIV = AES.SZ_IV;
        int osSaltAES = osIV + szIV;
        int szSaltAES = AES.SZ_AES_SALT;
        int osSaltKDF = osSaltAES + szSaltAES;
        int szSaltKDF = AES.SZ_KDF_SALT;
        int osExtHeadSz = osSaltKDF + szSaltKDF;
        int szExtHeadSz = 2;
        int szHeader = szVersion + szIV + szSaltAES + szSaltKDF + szExtHeadSz;

        byte[] header = new byte[szHeader];
        readBytes = Reader.readChunk(szHeader, header);
        if (szHeader != readBytes) {
            throw new IOException("Invalid file.");
        }

        mVersion = Format.Ascii.string(Arrays.copyOfRange(header, osVersion, osVersion + szVersion));
        byte[] iv = Arrays.copyOfRange(header, osIV, osIV + szIV);
        mSaltAES = Arrays.copyOfRange(header, osSaltAES, osSaltAES + szSaltAES);
        byte[] saltKDF = Arrays.copyOfRange(header, osSaltKDF, osSaltKDF + szSaltKDF);
        short szExtHead = Format.Short.convert(Arrays.copyOfRange(header, osExtHeadSz, osExtHeadSz + szExtHeadSz));

        // AES Initialization
        AES.init(AES.DECODE, mPassword, saltKDF, mSaltAES, iv);

        // Decrypt ext header
        int osSanity = 0;
        int szSanity = Config.SANITY.length();
        int osSaltHMAC = osSanity + szSanity;
        int szSaltHMAC = HMAC.SZ_KDF_SALT;

        byte[] data = new byte[szExtHead];
        readBytes = Reader.readChunk(szExtHead, data);
        if (szExtHead != readBytes) {
            throw new IOException("Invalid file.");
        }
        data = AES.update(data, mSaltAES);

        String sanity = Format.Ascii.string(Arrays.copyOfRange(data, osSanity, osSanity + szSanity));
        if (!sanity.equals(Config.SANITY)) {
            throw new InvalidKeyException("Sanity failed. Is password valid?");
        }

        byte[] saltHMAC = Arrays.copyOfRange(data, osSaltHMAC, osSaltHMAC + szSaltHMAC);

        int osFilename = osSaltHMAC + szSaltHMAC;
        byte szFilename = data[osFilename];
        osFilename++;
        mFileName = Format.UTF8.string(Arrays.copyOfRange(data, osFilename, osFilename + szFilename));

        int osTitle = osFilename + szFilename;
        byte szTitle = data[osTitle];
        osTitle++;
        mTitle = Format.UTF8.string(Arrays.copyOfRange(data, osTitle, osTitle + szTitle));

        // HMAC Initialization
        HMAC.init(mPassword, saltHMAC);

        mDataSize = mFileSize - szHeader - szExtHead - HMAC.SZ_BYTES;
        mProcessedSize = 0;

        callback.updateProgress(5, "Header received");
    }

    /**
     * Reads data
     * @return size of processed data
     * @throws IOException
     */
    public static int updateData()
    throws IOException {
        long totalBytes = mDataSize - mProcessedSize;
        byte[] data = new byte[Config.SZ_CHUNK];
        int chunkSize = Config.SZ_CHUNK;
        if (chunkSize > totalBytes) {
            chunkSize = (int) totalBytes;
        }
        int readBytes = Reader.readChunk(chunkSize, data);
        if (0 < readBytes) {
            byte[] buffer = new byte[readBytes];
            System.arraycopy(data, 0, buffer, 0, readBytes);
            buffer = AES.update(buffer, mSaltAES);
            HMAC.update(buffer);
            Writer.writeChunk(buffer, readBytes);
        }
        mProcessedSize += readBytes;

        int value = (int) (5 + ((float) mProcessedSize / (float) mDataSize) * 90.0);
        String progress = readableFileSize(mProcessedSize) + " / " + readableFileSize(mDataSize) + " received";
        callback.updateProgress(value, progress);

        return readBytes;
    }

    /**
     * Reads HMAC and compare it with computed HMAC
     * @throws IOException
     */
    public static void updateHMAC()
    throws IOException {
        byte[] hmac = new byte[HMAC.SZ_BYTES];
        int readBytes = Reader.readChunk(HMAC.SZ_BYTES, hmac);
        if (HMAC.SZ_BYTES != readBytes) {
            throw new IOException("Invalid file.");
        }
        hmac = AES.update(hmac, mSaltAES);

        // Compare HMAC
        byte[] computedHMAC = HMAC.finale();
        if (!Arrays.equals(hmac, computedHMAC)) {
            throw new IOException("Invalid file.");
        }

        callback.updateProgress(100, "Completed");
    }

    /**
     * Closes input and output streams
     * @throws IOException
     */
    public static void close()
    throws IOException {
        Writer.close();
        Reader.close();
    }

    /**
     * Return version
     * @return version
     */
    public static String version() {
        return mVersion;
    }

    /**
     * Returns target filename
     * @return filename
     */
    public static String fileName() {
        return mFileName;
    }

    /**
     * Returns title
     * @return title
     */
    public static String title() {
        return mTitle;
    }
}