package Encoder.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.DecimalFormat;

public class Format {

    public static class Hex {

        /**
         * Formats bytes[] to String
         * @param data Incoming bytes[]
         * @return Human readable string
         */
        public static String string(byte[] data) {
            return string(data, 256, "");
        }

        /**
         * Formats bytes[] to String
         * @param data Incoming bytes[]
         * @param padding Padding
         * @param tab Tab string
         * @return Human readable string
         */
        public static String string(byte[] data, int padding, String tab) {
            if (null == data) {
                return "NULL";
            }
            String HEX_DIGITS = "0123456789abcdef";
            StringBuilder buf = new StringBuilder();
            for (int i = 0; i != data.length; i++) {
                int v = data[i] & 0xff;
                buf.append(HEX_DIGITS.charAt(v >> 4));
                buf.append(HEX_DIGITS.charAt(v & 0xf));
                buf.append(" ");
                if (0 == ((i+1) % padding)) {
                    buf.append(tab);
                }
            }
            return buf.toString();
        }
    }

    public static class Ascii {

        /**
         * Returns ascii bytes from String
         * @param string string
         * @return string in ascii
         * @throws UnsupportedEncodingException
         */
        public static byte[] bytes(String string)
        throws UnsupportedEncodingException {
            return string.getBytes("US-ASCII");
        }

        /**
         * Returns String from ascii bytes
         * @param string bytes of string
         * @return string
         * @throws UnsupportedEncodingException
         */
        public static String string(byte[] string)
        throws UnsupportedEncodingException {
            return new String(string, "US-ASCII");
        }
    }

    public static class UTF8 {

        /**
         * Returns UTF8 bytes from String
         * @param string string
         * @return string in UTF8
         * @throws UnsupportedEncodingException
         */
        public static byte[] bytes(String string)
        throws UnsupportedEncodingException {
            return string.getBytes("UTF8");
        }

        /**
         * Returns String from UTF8 bytes
         * @param string bytes of string
         * @return string in UTF8
         * @throws UnsupportedEncodingException
         */
        public static String string(byte[] string)
        throws UnsupportedEncodingException {
            return new String(string, "UTF8");
        }
    }

    public static class UTF16BE {

        /**
         * Returns UTF16BE bytes from String
         * @param string string
         * @return string in UTF16BE
         * @throws UnsupportedEncodingException
         */
        public static byte[] bytes(String string)
        throws UnsupportedEncodingException {
            return string.getBytes("UTF-16BE");
        }
    }

    /**
     * Concatenates byte arrays
     * @param arrays byte arrays
     * @return concatenation of byte arrays
     * @throws IOException
     */
    public static byte[] concatBytes(byte[] ... arrays)
    throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        for (byte[] array : arrays) {
            outputStream.write(array);
        }
        return outputStream.toByteArray();
    }

    public static class Short {

        /**
         * Converts byte array to short
         * @param value byte array
         * @return short value
         */
        public static short convert (byte[] value) {
            ByteBuffer buffer = ByteBuffer.wrap(value);
            buffer.order(ByteOrder.BIG_ENDIAN);
            return buffer.getShort();
        }

        /**
         * Converts short to byte array
         * @param value short value
         * @return byte array
         */
        public static byte[] bytes(short value) {
            ByteBuffer buffer = ByteBuffer.allocate(2);
            buffer.order(ByteOrder.BIG_ENDIAN);
            buffer.putShort(value);
            return buffer.array();
        }
    }

    public static class Byte {

        /**
         * Converts byte to byte array
         * @param value byte value
         * @return byte array
         */
        public static byte[] bytes(byte value) {
            ByteBuffer buffer = ByteBuffer.allocate(1);
            buffer.order(ByteOrder.BIG_ENDIAN);
            buffer.put(value);
            return buffer.array();
        }
    }

    /**
     * Returns readable file size
     * @param size File size in bytes
     * @return Readable file size
     */
    public static String readableFileSize(long size) {
        if(size <= 0) return "0";
        final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }
}