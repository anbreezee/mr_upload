package Encoder;

import Encoder.crypt.AES;
import Encoder.crypt.HMAC;
import Encoder.io.Reader;
import Encoder.io.Writer;
import Encoder.utils.Format;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.zip.Checksum;

import static Encoder.utils.Format.readableFileSize;

public class Encoder {

    private static String mFileName;
    private static String mTitle;

    private static ProgressCallback callback;
    private static long mFileSize;
    private static long mProcessedSize;

    /**
     * initialize()
     * Initialization
     * @param in input stream
     * @param out output stream
     * @param password password
     * @param filename filename
     * @param title title
     * @throws InvalidKeyException
     * @throws InvalidKeySpecException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
     public static void initialize(InputStream in, OutputStream out, long fileSize, String password, String filename,
                                   String title, ProgressCallback callback)
     throws InvalidKeyException, InvalidKeySpecException, InvalidAlgorithmParameterException,
            NoSuchAlgorithmException, NoSuchPaddingException {
        mFileName = filename;
        mTitle = title;
        mFileSize = fileSize;
        Encoder.callback = callback;

        callback.updateProgress(0, "Initializing...");
        Reader.init(in);
        Writer.init(out);
        HMAC.init(password);
        AES.init(AES.ENCODE, password);
        callback.updateProgress(1, "Starting... Please wait");
    }

    /**
     * updateHeader()
     * Writes header
     * @throws IOException
     */
    public static void updateHeader()
    throws IOException {

        // Header: version + iv + aes salt + kdf salt
        byte[] head = Format.concatBytes(Format.Ascii.bytes(Config.VERSION), AES.iv(), AES.saltAES(), AES.saltKDF());

        // Sanity
        byte[] sane = Format.Ascii.bytes(Config.SANITY);

        // Filename
        byte[] filename = Format.UTF8.bytes(mFileName);
        byte szFilename = (byte) filename.length;
        filename = Format.concatBytes(Format.Byte.bytes(szFilename), filename);

        // Title
        byte[] title = Format.UTF8.bytes(mTitle);
        byte szTitle = (byte) title.length;
        title = Format.concatBytes(Format.Byte.bytes(szTitle), title);

        // Write: Header + AES[Sanity + HMAC Salt + Filename + Title]
        byte[] extHeader = AES.update(Format.concatBytes(sane, HMAC.salt(), filename, title), AES.saltAES());
        short szExtHead = (short) extHeader.length;

        byte[] dataToWrite = Format.concatBytes(head, Format.Short.bytes(szExtHead), extHeader);
        Writer.writeChunk(dataToWrite);

        mProcessedSize = 0;

        callback.updateProgress(5, "Header sent");
    }

    /**
     * updateData()
     * Writes data
     * @return length of read bytes
     * @throws IOException
     */
    public static int updateData()
    throws IOException {
        long totalBytes = mFileSize - mProcessedSize;
        byte[] data = new byte[Config.SZ_CHUNK];
        int readBytes = Reader.readChunk(Config.SZ_CHUNK, data);
        if (0 < readBytes) {
            byte[] buffer = new byte[readBytes];
            System.arraycopy(data, 0, buffer, 0, readBytes);
            HMAC.update(buffer);

            byte[] dataToWrite = AES.update(buffer, AES.saltAES());
            Writer.writeChunk(dataToWrite);
        }
        mProcessedSize += readBytes;

        int value = (int) (5 + ((float) mProcessedSize / (float) mFileSize) * 90.0);
        String progress = readableFileSize(mProcessedSize) + " / " + readableFileSize(mFileSize) + " sent";
        callback.updateProgress(value, progress);

        return readBytes;
    }

    /**
     * updateHMAC()
     * Writes HMAC
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws IOException
     */
    public static void updateHMAC()
    throws IllegalBlockSizeException, BadPaddingException, IOException {
        // HMAC
        byte[] hmac = AES.update(HMAC.finale(), AES.saltAES());

        byte[] dataToWrite = Format.concatBytes(hmac, AES.finale());
        Writer.writeChunk(dataToWrite);

        callback.updateProgress(100, "Completed");
    }

    /**
     * close()
     * Closes input and output streams
     * @throws IOException
     */
    public static void close()
    throws IOException {
        Reader.close();
        Writer.close();
    }
}