package Encoder.crypt;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

public class PBKDF2 {

    /**
     * PBKDF2 generator
     * @param password Password
     * @param salt Salt
     * @param iterations Iterations count
     * @param length Key length
     * @return Key bytes
     * @throws java.security.NoSuchAlgorithmException
     * @throws java.security.spec.InvalidKeySpecException
     */
    public static Key init(char[] password, byte[] salt, int iterations, int length)
    throws NoSuchAlgorithmException, InvalidKeySpecException {
        SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec ks = new PBEKeySpec(password, salt, iterations, length);
        SecretKey s = f.generateSecret(ks);
        return new SecretKeySpec(s.getEncoded(), "AES");
    }
}
