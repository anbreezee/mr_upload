package Encoder.crypt;

import Encoder.utils.Format;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public class HMAC {

    public static final int SZ_KDF_SALT = 32;
    private static final int SZ_KDF_ITERATIONS = 4096;
    private static final int SZ_KDF_KEY_BITS = 256;
    public static final int SZ_BYTES = 32;
    private static final String ALG = "HMACSHA256";

    private static byte[] mSalt;
    private static Key mKey;
    private static Mac mMac = null;

    /**
     * Initialization
     * @param password Password
     * @throws InvalidKeyException
     * @throws InvalidKeySpecException
     * @throws NoSuchAlgorithmException
     */
    public static void init(String password)
    throws InvalidKeyException, InvalidKeySpecException, NoSuchAlgorithmException {
        init(password, Salt.init(SZ_KDF_SALT));
    }

    /**
     * Initialization
     * @param password Password
     * @param salt Salt
     * @throws InvalidKeySpecException
     * @throws InvalidKeyException
     * @throws NoSuchAlgorithmException
     */
    public static void init(String password, byte[] salt)
    throws InvalidKeySpecException, InvalidKeyException, NoSuchAlgorithmException{
        mSalt = salt;
        mKey = PBKDF2.init(password.toCharArray(), salt, SZ_KDF_ITERATIONS, SZ_KDF_KEY_BITS);

        // HMAC
        SecretKey mSigningKey = new SecretKeySpec(mKey.getEncoded(), ALG);
        mMac = Mac.getInstance(ALG);
        mMac.init(mSigningKey);
    }

    /**
     * @param data #desc#
     */
    public static void update(byte[] data) {
        if (null == mMac) {
            return;
        }
        mMac.update(data);
    }

    /**
     * @return #desc#
     */
    public static byte[] finale() {
        if (null == mMac) {
            return null;
        }
        return mMac.doFinal();
    }

    /**
     * @return Salt
     */
    public static byte[] salt() {
        return mSalt;
    }

    /**
     * Displays the report
     */
    public static void report() {
        System.out.printf("=== HMAC ===%n%n");
        System.out.printf("Salt:\t\t%s%n", Format.Hex.string(mSalt, 16, "\n\t\t"));
        System.out.printf("PBKDF2:\t\t%s%n", Format.Hex.string(mKey.getEncoded(), 16, "\n\t\t"));
    }
}