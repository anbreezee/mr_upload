package Encoder.crypt;

import java.util.Random;

public class IV {

    // Initialization vector
    private static byte[] mIV;

    /**
     * IV Generator
     * @param length Vector length
     * @return Vector bytes
     */
    public static byte[] init(int length) {
        mIV = new byte[length];
        Random randomGenerator = new Random();
        randomGenerator.nextBytes(mIV);
        return mIV;
    }

    /**
     * Initialization vector
     * @return Vector bytes
     */
    public static byte[] iv() {
        return mIV;
    }
}