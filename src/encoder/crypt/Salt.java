package Encoder.crypt;

import java.util.Random;

public class Salt {

    /**
     * Salt generator
     * @param length Salt bytes length
     * @return Salt bytes
     */
    public static byte[] init(int length) {
        byte[] salt = new byte[length];
        Random randomGenerator = new Random();
        randomGenerator.nextBytes(salt);
        return salt;
    }
}