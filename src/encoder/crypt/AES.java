package Encoder.crypt;

import Encoder.utils.Format;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public class AES {

    private static final String ALG = "AES/CTR/NoPadding";
    public static final int ENCODE = Cipher.ENCRYPT_MODE;
    public static final int DECODE = Cipher.DECRYPT_MODE;

    public static final int SZ_IV = 16;
    public static final int SZ_AES_SALT = 16;
    public static final int SZ_KDF_SALT = 16;
    private static final int SZ_KDF_ITERATIONS = 4096;
    private static final int SZ_KDF_KEY_BITS = 128;

    private static int mMode;
    private static Cipher mCipher = null;
    private static byte[] mSaltAES;
    private static byte[] mSaltKDF;
    private static byte[] mIV;
    private static Key mKey;
    private static int mSaltOffset = 0;

    /**
     * Initialization
     * @param mode Encryption mode
     * @param password Password
     * @throws InvalidKeySpecException
     * @throws InvalidKeyException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    public static void init(int mode, String password)
    throws InvalidKeySpecException, InvalidKeyException, InvalidAlgorithmParameterException,
           NoSuchAlgorithmException, NoSuchPaddingException {
        init(mode, password, Salt.init(SZ_KDF_SALT), Salt.init(SZ_AES_SALT), IV.init(SZ_IV));
    }

    /**
     * @param mode Mode
     * @param password Password
     * @param saltAES AES Salt
     * @param saltKDF KDF Salt
     * @param iv IV
     * @throws InvalidKeySpecException
     * @throws InvalidKeyException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    public static void init(int mode, String password, byte[] saltKDF, byte[] saltAES, byte[] iv)
    throws InvalidKeySpecException, InvalidKeyException, InvalidAlgorithmParameterException,
           NoSuchAlgorithmException, NoSuchPaddingException {
        mMode = mode;
        mSaltKDF = saltKDF;
        mSaltAES = saltAES;
        mIV = iv;
        mKey = PBKDF2.init(password.toCharArray(), saltKDF, SZ_KDF_ITERATIONS, SZ_KDF_KEY_BITS);
        mSaltOffset = 0;

        // AES
        mCipher = Cipher.getInstance(ALG);
        IvParameterSpec ivSpec = new IvParameterSpec(mIV);
        if (ENCODE == mMode || DECODE == mMode) {
            mCipher.init(mode, mKey, ivSpec);
        }
    }

    /**
     * @param data Data
     * @param salt Salt
     * @return TODO add comment
     */
    public static byte[] update(byte[] data, byte[] salt) {
        if (null == mCipher) return null;
        if (0 == data.length) return data;
        if (mMode == AES.ENCODE) {
            return mCipher.update(xorWithKey(data, salt));
        } else if (mMode == AES.DECODE) {
            return xorWithKey(mCipher.update(data), salt);
        }
        return null;
    }

    /**
     * XOR byte array with key
     * @param a Source array
     * @param key Key
     * @return Result array
     */
    private static byte[] xorWithKey(byte[] a, byte[] key) {
        byte[] out = new byte[a.length];
        for (int i = 0; i < a.length; i++) {
            out[i] = (byte) (a[i] ^ key[(i + mSaltOffset) % key.length]);
        }
        mSaltOffset = (mSaltOffset + a.length) % key.length;
        return out;
    }

    /**
     * @return #desc#
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    public static byte[] finale()
    throws IllegalBlockSizeException, BadPaddingException {
        if (null == mCipher) return null;
        return mCipher.doFinal();
    }

    /**
     * Displays the report
     */
    public static void report() {
        System.out.printf("=== AES ===%n%n");
        System.out.printf("Salt:\t\t%s%n", Format.Hex.string(mSaltKDF));
        System.out.printf("PBKDF2:\t\t%s%n", Format.Hex.string(mKey.getEncoded()));
        System.out.printf("IV:\t\t%s%n", Format.Hex.string(mIV));
        System.out.printf("AES Salt:\t%s%n", Format.Hex.string(mSaltAES));
    }

    /**
     * @return AES Salt
     */
    public static byte[] saltAES() {
        return mSaltAES;
    }

    /**
     * @return KDF Salt
     */
    public static byte[] saltKDF() {
        return mSaltKDF;
    }

    /**
     * @return Initialization vector
     */
    public static byte[] iv() {
        return mIV;
    }
}