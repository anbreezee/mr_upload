package Encoder;

public class Config {

    public static final String VERSION = "MR01";
    public static final String SANITY = "IAMSANE";

    // 40 kilobytes in chunk
    public static final int SZ_CHUNK = 32768;
}
