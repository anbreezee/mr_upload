package Encoder.io;

import java.io.*;

public class Reader {

    private static InputStream mIS;

    /**
     * Initialization
     * @param is Input stream
     */
    public static void init(InputStream is) {
        mIS = is;
    }

    /**
     * Reads chunk of data from reader
     * @param maxSize #desc#
     * @param res #desc#
     * @return #desc#
     * @throws IOException
     */
    public static int readChunk(int maxSize, byte[] res)
    throws IOException {
        return mIS.read(res, 0, maxSize);
    }

    /**
     * Closes input stream
     * @throws IOException
     */
    public static void close()
    throws IOException {
        if (null != mIS) {
            mIS.close();
        }
        mIS = null;
    }
}